* [XToys](https://xtoys.app/)
* [Chrome](https://www.google.com/intl/zh-CN/chrome/)
* [Edge](https://www.microsoft.com/zh-cn/edge)
* [连接 XToys 必须用的 Bondage Club](https://bondageprojects.elementfx.com/R79/BondageClub)
* [XToys 扩展](https://chrome.google.com/webstore/detail/xtoys/hhddbheipffnedlmnefnkjeplkgbajij)
