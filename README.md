# 紫猫的郊狼连接笔记

* [点击可直接查看](https://gitlab.com/nekonya-bc/bc-drive/-/blob/main/BC-XToys-DGLab/%E5%85%B3%E4%BA%8E%E7%8C%AB%E5%A6%82%E4%BD%95%E8%BF%9E%E6%8E%A5%E4%B8%8A%E9%83%8A%E7%8B%BC%E7%9A%84_%E5%9C%A8%E7%BA%BF%E6%9F%A5%E7%9C%8B_.pdf)

* [觉得模糊的话来这里下载高清版](https://gitlab.com/nekonya-bc/bc-drive/-/blob/main/BC-XToys-DGLab/%E7%AC%A8%E8%9B%8B%E7%B4%AB%E7%8C%AB%E6%AD%A3%E5%9C%A8%E6%8E%A2%E7%B4%A2%E6%80%8E%E4%B9%88%E7%8E%A9%E5%9D%8F%E8%87%AA%E5%B7%B1%E5%96%B5.pdf)

# 你可能需要的链接

* [XToys](https://xtoys.app/)
* [Chrome](https://www.google.com/intl/zh-CN/chrome/)
* [Edge](https://www.microsoft.com/zh-cn/edge)
* [连接 XToys 必须用的 Bondage Club](https://bondageprojects.elementfx.com/R79/BondageClub)
* [XToys 扩展](https://chrome.google.com/webstore/detail/xtoys/hhddbheipffnedlmnefnkjeplkgbajij)
